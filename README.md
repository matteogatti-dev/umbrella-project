# Umbrella Project

### Overview
The whole project run in multiple docker containers where API are reachable on **http://localhost:80**
while the Frontend is on **http://localhost:9000**

[**DEMO APP**](http://umbrella.developer-games.cloud)

### Database schema
```
SETTING
--------
id
beach_name
beach_rows
beach_columns
umbrella_positions
created_at
updated_at
```
```
UMBRELLA
--------
id
position_index
state
price
label
start_reservation
end_reservation
created_at
updated_at
customer_id
setting_id
```
```
CUSTOMER
--------
id
first_name
last_name
phone
created_at
updated_at
```

### Docker useful commands
- Start enviroments ``docker-compose up -d``
- Enter in the container ``docker exec -it deploy_php bash`` or ``docker exec -it deploy_react /bin/sh``