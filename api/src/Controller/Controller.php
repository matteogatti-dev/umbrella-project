<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class Controller extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        return new JsonResponse('UP');
    }

    /**
     * @param array $content
     * @return JsonResponse
     */
    protected function toJSON($content)
    {
        return new JsonResponse($content);
    }
}
