<?php

namespace App\Controller;

use App\Factory\CustomerFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class CustomerController extends Controller
{
    /**
     * @param int $id
     * @param CustomerFactory $customerFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/customer/{id}.json", name="customer_retrieve", methods={"GET"})
     */
    public function retrieve($id, CustomerFactory $customerFactory)
    {
        try {
            return $this->toJSON([
                'success' => true,
                'customer' => $customerFactory->retrieve($id)
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param Request $request
     * @param CustomerFactory $customerFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/customer.json", name="customer_create", methods={"POST"})
     */
    public function create(Request $request, CustomerFactory $customerFactory)
    {
        $params = new ArrayCollection(json_decode($request->getContent(), true));

        try {
            $customer = $customerFactory->create($params);

            return $this->toJSON([
                'success' => true,
                'customer' => $customer
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @param CustomerFactory $customerFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/customer/{id}.json", name="customer_update", methods={"PUT"})
     */
    public function update($id, Request $request, CustomerFactory $customerFactory)
    {
        $params = new ArrayCollection(json_decode($request->getContent(), true));

        try {
            $customer = $customerFactory->update($id, $params);

            return $this->toJSON([
                'success' => true,
                'customer' => $customer
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param int $id
     * @param CustomerFactory $customerFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/customer/{id}.json", name="customer_delete", methods={"DELETE"})
     */
    public function delete($id, CustomerFactory $customerFactory)
    {
        return $this->toJSON([
            'success' => $customerFactory->delete($id)
        ]);
    }
}
