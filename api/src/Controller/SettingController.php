<?php

namespace App\Controller;

use App\Factory\SettingFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class SettingController extends Controller
{
    /**
     * @param SettingFactory $settingFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/setting.json", name="setting_list", methods={"GET"})
     */
    public function list(SettingFactory $settingFactory)
    {
        return $this->toJSON([
            'success'  => true,
            'settings' => $settingFactory->list()
        ]);
    }

    /**
     * @param int $id
     * @param SettingFactory $settingFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/setting/{id}.json", name="setting_retrieve", methods={"GET"})
     */
    public function retrieve($id, SettingFactory $settingFactory)
    {
        try {
            return $this->toJSON([
                'success' => true,
                'setting' => $settingFactory->retrieve($id)
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param Request $request
     * @param SettingFactory $settingFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/setting.json", name="setting_create", methods={"POST"})
     */
    public function create(Request $request, SettingFactory $settingFactory)
    {
        $params = new ArrayCollection(json_decode($request->getContent(), true));

        try {
            $setting = $settingFactory->create($params);

            return $this->toJSON([
                'success' => true,
                'setting' => $setting
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @param SettingFactory $settingFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/setting/{id}.json", name="setting_update", methods={"PUT"})
     */
    public function update($id, Request $request, SettingFactory $settingFactory)
    {
        $params = new ArrayCollection(json_decode($request->getContent(), true));

        try {
            $setting = $settingFactory->update($id, $params);

            return $this->toJSON([
                'success' => true,
                'setting' => $setting
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param int $id
     * @param SettingFactory $settingFactory
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/setting/{id}.json", name="setting_delete", methods={"DELETE"})
     */
    public function delete($id, SettingFactory $settingFactory)
    {
        return $this->toJSON([
            'success' => $settingFactory->delete($id)
        ]);
    }
}
