<?php

namespace App\Controller;

use App\Factory\UmbrellaFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class UmbrellaController extends Controller
{
    /**
     * @param int $id
     * @param UmbrellaFactory $umbrellaFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/umbrella/{id}.json", name="umbrella_retrieve", methods={"GET"})
     */
    public function retrieve($id, UmbrellaFactory $umbrellaFactory)
    {
        try {
            return $this->toJSON([
                'success' => true,
                'umbrella' => $umbrellaFactory->retrieve($id)
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param Request $request
     * @param UmbrellaFactory $umbrellaFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/umbrella.json", name="umbrella_create", methods={"POST"})
     */
    public function create(Request $request, UmbrellaFactory $umbrellaFactory)
    {
        $params = new ArrayCollection(json_decode($request->getContent(), true));

//        try {
            $umbrella = $umbrellaFactory->create($params);

            return $this->toJSON([
                'success' => true,
                'umbrella' => $umbrella
            ]);
//        } catch (\Exception $e) {
//            return $this->toJSON(['success' => false]);
//        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @param UmbrellaFactory $umbrellaFactory
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/umbrella/{id}.json", name="umbrella_update", methods={"PUT"})
     */
    public function update($id, Request $request, UmbrellaFactory $umbrellaFactory)
    {
        $params = new ArrayCollection(json_decode($request->getContent(), true));

        try {
            $umbrella = $umbrellaFactory->update($id, $params);

            return $this->toJSON([
                'success' => true,
                'umbrella' => $umbrella
            ]);
        } catch (\Exception $e) {
            return $this->toJSON(['success' => false]);
        }
    }

    /**
     * @param int $id
     * @param UmbrellaFactory $umbrellaFactory
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/umbrella/{id}.json", name="umbrella_delete", methods={"DELETE"})
     */
    public function delete($id, UmbrellaFactory $umbrellaFactory)
    {
        return $this->toJSON([
            'success' => $umbrellaFactory->delete($id)
        ]);
    }
}
