<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Umbrella", mappedBy="customer")
     */
    private $umbrellas;

    public function __construct()
    {
        $this->umbrellas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|Umbrella[]
     */
    public function getUmbrellas(): Collection
    {
        return $this->umbrellas;
    }

    public function addUmbrella(Umbrella $umbrella): self
    {
        if (!$this->umbrellas->contains($umbrella)) {
            $this->umbrellas[] = $umbrella;
            $umbrella->setCustomer($this);
        }

        return $this;
    }

    public function removeUmbrella(Umbrella $umbrella): self
    {
        if ($this->umbrellas->contains($umbrella)) {
            $this->umbrellas->removeElement($umbrella);
            // set the owning side to null (unless already changed)
            if ($umbrella->getCustomer() === $this) {
                $umbrella->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPrePersist()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdate()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @return array
     */
    public function getPresenter()
    {
        return [
            'id'            => $this->getId(),
            'first_name'    => $this->getFirstName(),
            'last_name'     => $this->getLastName(),
            'phone'         => $this->getPhone(),
            'umbrellas'     => $this->getUmbrellas()->toArray()
        ];
    }
}
