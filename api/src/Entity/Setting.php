<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SettingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Setting
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $beach_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $beach_rows;

    /**
     * @ORM\Column(type="integer")
     */
    private $beach_columns;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $umbrella_positions;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Umbrella", mappedBy="setting", orphanRemoval=true)
     */
    private $umbrellas;

    public function __construct()
    {
        $this->umbrellas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeachName(): ?string
    {
        return $this->beach_name;
    }

    public function setBeachName(string $beach_name): self
    {
        $this->beach_name = $beach_name;

        return $this;
    }

    public function getBeachRows(): ?int
    {
        return $this->beach_rows;
    }

    public function setBeachRows(int $beach_rows): self
    {
        $this->beach_rows = $beach_rows;

        return $this;
    }

    public function getBeachColumns(): ?int
    {
        return $this->beach_columns;
    }

    public function setBeachColumns(int $beach_columns): self
    {
        $this->beach_columns = $beach_columns;

        return $this;
    }

    public function getUmbrellaPositions(): ?string
    {
        return $this->umbrella_positions;
    }

    public function setUmbrellaPositions(string $umbrella_positions): self
    {
        $this->umbrella_positions = $umbrella_positions;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|Umbrella[]
     */
    public function getUmbrellas(): Collection
    {
        return $this->umbrellas;
    }

    public function addUmbrella(Umbrella $umbrella): self
    {
        if (!$this->umbrellas->contains($umbrella)) {
            $this->umbrellas[] = $umbrella;
            $umbrella->setSetting($this);
        }

        return $this;
    }

    public function removeUmbrella(Umbrella $umbrella): self
    {
        if ($this->umbrellas->contains($umbrella)) {
            $this->umbrellas->removeElement($umbrella);
            // set the owning side to null (unless already changed)
            if ($umbrella->getSetting() === $this) {
                $umbrella->setSetting(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPrePersist()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdate()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @return array
     */
    public function getPresenter()
    {
        return [
            'id'                    => $this->getId(),
            'beach_name'            => $this->getBeachName(),
            'beach_rows'            => $this->getBeachRows(),
            'beach_columns'         => $this->getBeachColumns(),
            'umbrella_positions'    => $this->getUmbrellaPositions()
        ];
    }
}
