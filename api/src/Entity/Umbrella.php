<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UmbrellaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Umbrella
{
    const RESERVED_STATE    = 1;
    const BOOKABLE_STATE    = 0;
    const UNAVAILABLE_STATE = -1;

    const RESERVED_STATE_LABEL    = 'reserved';
    const BOOKABLE_STATE_LABEL    = 'bookable';
    const UNAVAILABLE_STATE_LABEL = 'unavailable';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $position_index;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $state;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $start_reservation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end_reservation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="umbrellas")
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Setting", inversedBy="umbrellas")
     * @ORM\JoinColumn(nullable=true)
     */
    private $setting;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPositionIndex(): ?int
    {
        return $this->position_index;
    }

    public function setPositionIndex(int $position_index): self
    {
        $this->position_index = $position_index;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStateLabel()
    {
        if ($this->state == 0) {
            return self::BOOKABLE_STATE_LABEL;
        }

        if ($this->state == 1) {
            return self::RESERVED_STATE_LABEL;
        }

        return self::UNAVAILABLE_STATE_LABEL;
    }

    public function setStateAsReserved()
    {
        $this->setState(self::RESERVED_STATE);
    }

    public function setStateAsBookable()
    {
        $this->setState(self::BOOKABLE_STATE);
    }

    public function setStateAsUnavailable()
    {
        $this->setState(self::UNAVAILABLE_STATE);
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getStartReservation(): ?\DateTimeInterface
    {
        return $this->start_reservation;
    }

    public function setStartReservation(?\DateTimeInterface $start_reservation): self
    {
        $this->start_reservation = $start_reservation;

        return $this;
    }

    public function getEndReservation(): ?\DateTimeInterface
    {
        return $this->end_reservation;
    }

    public function setEndReservation(?\DateTimeInterface $end_reservation): self
    {
        $this->end_reservation = $end_reservation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getSetting(): ?Setting
    {
        return $this->setting;
    }

    public function setSetting(?Setting $setting): self
    {
        $this->setting = $setting;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPrePersist()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdate()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @return array
     */
    public function getPresenter()
    {
        return [
            'id'                 => $this->getId(),
            'state'              => $this->getStateLabel(),
            'position_index'     => $this->getPositionIndex(),
            'label'              => $this->getLabel(),
            'price'              => $this->getPrice(),
            'start_reservation'  => $this->getStartReservation(),
            'end_reservation'    => $this->getEndReservation(),
            'customer'           => $this->getCustomerPresenter(),
            'umbrella_positions' => $this->getSetting()->getUmbrellaPositions(),
        ];
    }

    public function getCustomerPresenter()
    {
        $customer = $this->getCustomer();

        if (!$customer) {
            return null;
        }

        return  [
            'id'            => $customer->getId(),
            'first_name'    => $customer->getFirstName(),
            'last_name'     => $customer->getLastName(),
            'phone'         => $customer->getPhone(),
        ];
    }
}
