<?php

namespace App\Factory;

use App\Entity\Customer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Serializer\SerializerInterface;

class CustomerFactory
{
    const DATA_FORMAT = 'json';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }
    
    /**
     * @param int $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        /** @var Customer $customer */
        $customer = $this->entityManager
            ->getRepository(Customer::class)
            ->find($id);

        if (!$customer) {
            return json_encode(new \stdClass);
        }

        return $customer->getPresenter();
    }

    /**
     * @param ArrayCollection $params
     *
     * @return array
     */
    public function create(ArrayCollection $params)
    {
        $params->set('umbrella_positions', implode(
            ',',
            array_fill(0, $params->get('beach_rows') * $params->get('beach_columns'), 0)
        ));

        $customer = $this->serializer->deserialize(json_encode($params->toArray()), Customer::class, self::DATA_FORMAT);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $customer->getPresenter();
    }

    /**
     * @param int $id
     * @param ArrayCollection $params
     *
     * @return array|bool
     *
     * @throws EntityNotFoundException
     */
    public function update($id, ArrayCollection $params)
    {
        /** @var Customer $customer */
        $customer = $this->entityManager
            ->getRepository(Customer::class)
            ->find($id);

        if (!$customer) {
            throw new EntityNotFoundException(sprintf('Entity Setting with id %s not found', $id));
        }

        $this->serializer->deserialize(
            json_encode($params->toArray()),
            Customer::class,
            'json',
            ['object_to_populate' => $customer]
        );

        $this->entityManager->flush();

        return $customer->getPresenter();
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        /** @var Customer $customer */
        $customer = $this->entityManager
            ->getRepository(Customer::class)
            ->find($id);

        if (!$customer) {
            return false;
        }

        $this->entityManager->remove($customer);
        $this->entityManager->flush();

        return true;
    }
}