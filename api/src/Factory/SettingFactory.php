<?php

namespace App\Factory;

use App\Entity\Setting;
use App\Repository\SettingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Serializer\SerializerInterface;

class SettingFactory
{
    const DATA_FORMAT = 'json';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @return array
     */
    public function list()
    {
        /** @var SettingRepository $repository */
        $repository = $this->entityManager->getRepository(Setting::class);

        return $repository->findAllViaPresenter();
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        /** @var Setting $setting */
        $setting = $this->entityManager
            ->getRepository(Setting::class)
            ->find($id);

        if (!$setting) {
            return json_encode(new \stdClass);
        }

        return $setting->getPresenter();
    }

    /**
     * @param ArrayCollection $params
     *
     * @return array
     */
    public function create(ArrayCollection $params)
    {
        $params->set('umbrella_positions', implode(
            ',',
            array_fill(0, $params->get('beach_rows') * $params->get('beach_columns'), 0)
        ));

        $setting = $this->serializer->deserialize(json_encode($params->toArray()), Setting::class, self::DATA_FORMAT);

        $this->entityManager->persist($setting);
        $this->entityManager->flush();

        return $setting->getPresenter();
    }

    /**
     * @param int $id
     * @param ArrayCollection $params
     *
     * @return array|bool
     *
     * @throws EntityNotFoundException
     */
    public function update($id, ArrayCollection $params)
    {
        /** @var Setting $setting */
        $setting = $this->entityManager
            ->getRepository(Setting::class)
            ->find($id);

        if (!$setting) {
            throw new EntityNotFoundException(sprintf('Entity Setting with id %s not found', $id));
        }

        $this->serializer->deserialize(
            json_encode($params->toArray()),
            Setting::class,
            'json',
            ['object_to_populate' => $setting]
        );

        $this->entityManager->flush();

        return $setting->getPresenter();
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        /** @var Setting $setting */
        $setting = $this->entityManager
            ->getRepository(Setting::class)
            ->find($id);

        if (!$setting) {
            return false;
        }

        $this->entityManager->remove($setting);
        $this->entityManager->flush();

        return true;
    }
}