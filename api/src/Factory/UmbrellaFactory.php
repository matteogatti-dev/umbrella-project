<?php

namespace App\Factory;

use App\Entity\Setting;
use App\Entity\Umbrella;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\Serializer\SerializerInterface;

class UmbrellaFactory
{
    const DATA_FORMAT = 'json';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        /** @var Umbrella $umbrella */
        $umbrella = $this->entityManager
            ->getRepository(Umbrella::class)
            ->find($id);

        if (!$umbrella) {
            return json_encode(new \stdClass);
        }

        return $umbrella->getPresenter();
    }

    /**
     * @param ArrayCollection $params
     *
     * @return array
     *
     * @throws ParameterNotFoundException
     * @throws EntityNotFoundException
     */
    public function create(ArrayCollection $params)
    {
        if (!$params->get('setting')) {
            throw new ParameterNotFoundException('setting');
        }

        /** @var Setting $setting */
        $setting = $this->entityManager
            ->getRepository(Setting::class)
            ->find($params->get('setting'));

        if (!$setting) {
            throw new EntityNotFoundException(sprintf('Entity Setting with id %s not found', $params->get('setting')));
        }

        /** @var Umbrella $deserializedUmbrella */
        $deserializedUmbrella = $this->serializer
            ->deserialize(
                json_encode($params->toArray()),
                Umbrella::class,
                self::DATA_FORMAT
            );

        $umbrella = $this->updateUmbrellaStateByPositionIndex(
            $deserializedUmbrella,
            $params->get('state'),
            $params->get('position_index')
        );

        $this->entityManager->persist($umbrella);
        $this->entityManager->flush();

        $this->entityManager->refresh($umbrella->getSetting());

        return $umbrella->getPresenter();
    }

    /**
     * @param int $id
     * @param ArrayCollection $params
     *
     * @return array
     *
     * @throws EntityNotFoundException
     */
    public function update($id, ArrayCollection $params)
    {
        /** @var Umbrella $umbrella */
        $umbrella = $this->entityManager
            ->getRepository(Umbrella::class)
            ->find($id);

        if (!$umbrella) {
            throw new EntityNotFoundException(sprintf('Entity Setting with id %s not found', $id));
        }

        $this->serializer->deserialize(
            json_encode($params->toArray()),
            Umbrella::class,
            'json',
            ['object_to_populate' => $umbrella]
        );

        $this->entityManager->flush();

        return $umbrella->getPresenter();
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        /** @var Umbrella $umbrella */
        $umbrella = $this->entityManager
            ->getRepository(Umbrella::class)
            ->find($id);

        if (!$umbrella) {
            return false;
        }

        $this->entityManager->remove($umbrella);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Umbrella $umbrella
     * @param string   $state
     * @param int      $position_index
     *
     * @return Umbrella
     */
    private function updateUmbrellaStateByPositionIndex($umbrella, $state, $position_index)
    {
        $positions = explode(',', $umbrella->getSetting()->getUmbrellaPositions());
        $positions[$position_index] = $state;

        $umbrella->getSetting()->setUmbrellaPositions(implode(',', $positions));

        return $umbrella;
    }
}