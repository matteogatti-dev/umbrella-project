<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190323102308 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE umbrella_umbrella (umbrella_source INT NOT NULL, umbrella_target INT NOT NULL, INDEX IDX_9F4DDEB86AA353 (umbrella_source), INDEX IDX_9F4DDEB118FF3DC (umbrella_target), PRIMARY KEY(umbrella_source, umbrella_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer_customer (customer_source INT NOT NULL, customer_target INT NOT NULL, INDEX IDX_ADFCCD26EE4FA322 (customer_source), INDEX IDX_ADFCCD26F7AAF3AD (customer_target), PRIMARY KEY(customer_source, customer_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE umbrella_umbrella ADD CONSTRAINT FK_9F4DDEB86AA353 FOREIGN KEY (umbrella_source) REFERENCES umbrella (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE umbrella_umbrella ADD CONSTRAINT FK_9F4DDEB118FF3DC FOREIGN KEY (umbrella_target) REFERENCES umbrella (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE customer_customer ADD CONSTRAINT FK_ADFCCD26EE4FA322 FOREIGN KEY (customer_source) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE customer_customer ADD CONSTRAINT FK_ADFCCD26F7AAF3AD FOREIGN KEY (customer_target) REFERENCES customer (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE umbrella_umbrella');
        $this->addSql('DROP TABLE customer_customer');
    }
}
