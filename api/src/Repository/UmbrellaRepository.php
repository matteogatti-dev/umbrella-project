<?php

namespace App\Repository;

use App\Entity\Umbrella;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Umbrella|null find($id, $lockMode = null, $lockVersion = null)
 * @method Umbrella|null findOneBy(array $criteria, array $orderBy = null)
 * @method Umbrella[]    findAll()
 * @method Umbrella[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UmbrellaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Umbrella::class);
    }

    // /**
    //  * @return Umbrella[] Returns an array of Umbrella objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Umbrella
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
