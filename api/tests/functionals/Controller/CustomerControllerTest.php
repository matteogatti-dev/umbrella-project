<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;

class CustomerControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public static function setUpBeforeClass()
    {
        $client = static::createClient();

        $application = new Application($client->getKernel());
        $application->setAutoExit(false);

        $application->run(new StringInput('doctrine:database:drop --force --quiet'));
        $application->run(new StringInput('doctrine:database:create --quiet'));
        $application->run(new StringInput('doctrine:schema:create --quiet'));
    }

    public function testCreateCustomer()
    {
        $this->client->request(
            'POST',
            '/api/customer.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"first_name":"Matteo","last_name":"Gatti"}'
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"customer":{"id":1,"first_name":"Matteo","last_name":"Gatti","phone":null,"umbrellas":[]}}', $this->client->getResponse()->getContent());
    }

    public function testRetrieveCustomer()
    {
        $this->client->request(
            'GET',
            '/api/customer/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"customer":{"id":1,"first_name":"Matteo","last_name":"Gatti","phone":null,"umbrellas":[]}}', $this->client->getResponse()->getContent());
    }

    public function testRetrieveNonExistentCustomer()
    {
        $this->client->request(
            'GET',
            '/api/customer/100.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"customer":"{}"}', $this->client->getResponse()->getContent());
    }

    public function testUpdateCustomer()
    {
        $this->client->request(
            'PUT',
            '/api/customer/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"phone":"3292950858"}'
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"customer":{"id":1,"first_name":"Matteo","last_name":"Gatti","phone":"3292950858","umbrellas":[]}}', $this->client->getResponse()->getContent());
    }

    public function testDeleteCustomer()
    {
        $this->client->request(
            'DELETE',
            '/api/customer/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true}', $this->client->getResponse()->getContent());
    }
}
