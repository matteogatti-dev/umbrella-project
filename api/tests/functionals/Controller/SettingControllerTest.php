<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;

class SettingControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public static function setUpBeforeClass()
    {
        $client = static::createClient();

        $application = new Application($client->getKernel());
        $application->setAutoExit(false);

        $application->run(new StringInput('doctrine:database:drop --force --quiet'));
        $application->run(new StringInput('doctrine:database:create --quiet'));
        $application->run(new StringInput('doctrine:schema:create --quiet'));
    }

    public function testCreateSetting()
    {
        $this->client->request(
            'POST',
            '/api/setting.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"beach_name":"Da Mario beach resort","beach_rows":10,"beach_columns":10}'
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"setting":{"id":1,"beach_name":"Da Mario beach resort","beach_rows":10,"beach_columns":10,"umbrella_positions":"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}}', $this->client->getResponse()->getContent());
    }

    public function testListSettings()
    {
        $this->client->request(
            'GET',
            '/api/setting.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"settings":[{"id":1,"beach_name":"Da Mario beach resort","beach_rows":10,"beach_columns":10,"umbrella_positions":"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}]}', $this->client->getResponse()->getContent());
    }

    public function testRetrieveSetting()
    {
        $this->client->request(
            'GET',
            '/api/setting/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"setting":{"id":1,"beach_name":"Da Mario beach resort","beach_rows":10,"beach_columns":10,"umbrella_positions":"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}}', $this->client->getResponse()->getContent());
    }

    public function testRetrieveNonExistentSetting()
    {
        $this->client->request(
            'GET',
            '/api/setting/100.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"setting":"{}"}', $this->client->getResponse()->getContent());
    }

    public function testUpdateSetting()
    {
        $this->client->request(
            'PUT',
            '/api/setting/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"beach_name":"Chalet Da Mario","umbrella_positions":"1,1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}'
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"setting":{"id":1,"beach_name":"Chalet Da Mario","beach_rows":10,"beach_columns":10,"umbrella_positions":"1,1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}}', $this->client->getResponse()->getContent());
    }

    public function testDeleteSetting()
    {
        $this->client->request(
            'DELETE',
            '/api/setting/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true}', $this->client->getResponse()->getContent());
    }
}