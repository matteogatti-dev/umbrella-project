<?php

namespace App\Tests\Controller;

use App\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use \Doctrine\ORM\EntityManager;

class UmbrellaControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    /** @var EntityManager */
    private $entityManager;

    public function setUp()
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public static function setUpBeforeClass()
    {
        $client = static::createClient();

        $application = new Application($client->getKernel());
        $application->setAutoExit(false);

        $application->run(new StringInput('doctrine:database:drop --force --quiet'));
        $application->run(new StringInput('doctrine:database:create --quiet'));
        $application->run(new StringInput('doctrine:schema:create --quiet'));
    }

    public function testCreateBaseSettingToHandleUmbrellaBehaviors()
    {
        /** SETTING */
        $this->client->request(
            'POST',
            '/api/setting.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"beach_name":"Da Mario beach resort","beach_rows":5,"beach_columns":5}'
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"setting":{"id":1,"beach_name":"Da Mario beach resort","beach_rows":5,"beach_columns":5,"umbrella_positions":"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}}', $this->client->getResponse()->getContent());
    }

    public function testCreateUmbrella()
    {
        $this->client->request(
            'POST',
            '/api/umbrella.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"position_index":1,"label":"201","price":12.5,"state":-1,"setting":1}'
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"umbrella":{"id":1,"state":"unavailable","position_index":1,"label":"201","price":12.5,"start_reservation":null,"end_reservation":null,"customer":null,"umbrella_positions":"0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}}', $this->client->getResponse()->getContent());
    }

    public function testRetrieveUmbrella()
    {
        $this->client->request(
            'GET',
            '/api/umbrella/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"umbrella":{"id":1,"state":"unavailable","position_index":1,"label":"201","price":12.5,"start_reservation":null,"end_reservation":null,"customer":null,"umbrella_positions":"0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}}', $this->client->getResponse()->getContent());
    }

    public function testUpdateUmbrella()
    {
        $this->createCustomer('Matteo', 'Gatti');

        $this->client->request(
            'PUT',
            '/api/umbrella/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"position_index":1,"state":1,"customer":1,"start_reservation":"2019-07-01","end_reservation":"2019-07-31"}'
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true,"umbrella":{"id":1,"state":"reserved","position_index":1,"label":"201","price":12.5,"start_reservation":{"date":"2019-07-01 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"end_reservation":{"date":"2019-07-31 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"customer":{"id":1,"first_name":"Matteo","last_name":"Gatti","phone":null},"umbrella_positions":"0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"}}', $this->client->getResponse()->getContent());
    }

    public function testDeleteUmbrella()
    {
        $this->client->request(
            'DELETE',
            '/api/umbrella/1.json',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('{"success":true}', $this->client->getResponse()->getContent());
    }

    private function createCustomer($firstName, $lastName = null, $phone = null)
    {
        $customer = new Customer();
        $customer->setFirstName($firstName);

        if ($lastName) {
            $customer->setLastName($lastName);
        }

        if ($phone) {
            $customer->setLastName($phone);
        }

        $this->entityManager->persist($customer);
        $this->entityManager->flush();
    }
}
