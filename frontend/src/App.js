import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Index from './containers/Index'
import Setting from './containers/Setting'
import Beach from './containers/Beach'
import Booking from './containers/Booking'

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Index} />
          <Route
            path="/setting/:id?"
            component={Setting}
          />
          <Route
            path="/beach/:id?"
            component={Beach}
          />
          <Route
            path="/booking/:id?"
            component={Booking}
          />
        </Switch>
      </Router>
    );
  }
}
export default connect()(App);