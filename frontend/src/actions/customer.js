import axios from 'axios';
import {
  CUSTOMER_CREATE_RESPONSE
} from '../actionTypes';

require('dotenv').config();

const customerCreateResponseAction = ({ error, customer, data }) => ({
  type: CUSTOMER_CREATE_RESPONSE,
  customer,
  data,
  error,
});

export const createCustomer = ({ data }) => dispatch =>
  new Promise((resolve, reject) => {
    axios.post(`${process.env.REACT_APP_API}/api/customer.json`, { ...data })
      .then(({ data }) => {
        if (!data.success) {
          return reject({ error: data.success });
        }

        dispatch(customerCreateResponseAction({ customer: data.customer, data }));
        resolve({ ...data });
      })
      .catch(error => {
        dispatch(customerCreateResponseAction({ error }));
        return reject({ error });
      });
  });