import axios from 'axios';
import {
  SETTING_LIST_REQUEST,
  SETTING_LIST_RESPONSE,
  SETTING_CREATE_RESPONSE,
  SETTING_UPDATE_RESPONSE,
  SETTING_DELETE_RESPONSE
} from '../actionTypes';

require('dotenv').config();

const settingListRequestAction = () => ({
  type: SETTING_LIST_REQUEST,
});

const settingListResponseAction = ({ error, settings, data }) => ({
  type: SETTING_LIST_RESPONSE,
  settings,
  data,
  error,
});

export const fetchSettings = () => dispatch =>
  new Promise((resolve, reject) => {
    dispatch(settingListRequestAction());

    axios.get(`${process.env.REACT_APP_API}/api/setting.json`)
      .then(({ data }) => {
        if (!data.success) {
          return reject({ error: data.success });
        }

        dispatch(settingListResponseAction({ settings: data.settings, data }));
        resolve({ ...data });
      })
      .catch(error => {
        dispatch(settingListResponseAction({ error }));
        return reject({ error });
      });
  });

export const fetchSettingsIfNeeded = () => (dispatch, getState) =>
  new Promise(resolve => {
    if (getState().setting.items.length > 0) {
      return resolve({ settings: getState().setting.items });
    }

    return dispatch(fetchSettings());
  });

const settingCreateResponseAction = ({ error, setting, data }) => ({
  type: SETTING_CREATE_RESPONSE,
  setting,
  data,
  error,
});

export const createSettings = ({ data }) => dispatch =>
  new Promise((resolve, reject) => {
    axios.post(`${process.env.REACT_APP_API}/api/setting.json`, { ...data })
      .then(({ data }) => {
        if (!data.success) {
          return reject({ error: data.success });
        }

        dispatch(settingCreateResponseAction({ settings: data.setting, data }));
        resolve({ ...data });
      })
      .catch(error => {
        dispatch(settingCreateResponseAction({ error }));
        return reject({ error });
      });
  });

const settingUpdateResponseAction = ({ error, setting, data }) => ({
  type: SETTING_UPDATE_RESPONSE,
  setting,
  data,
  error,
});

export const updateSettings = ({ id, data }) => dispatch =>
  new Promise((resolve, reject) => {
    axios.put(`${process.env.REACT_APP_API}/api/setting/${id}.json`, { ...data })
      .then(({ data }) => {
        if (!data.success) {
          return reject({ error: data.success });
        }

        dispatch(settingUpdateResponseAction({ setting: data.setting, data }));
        resolve({ ...data });
      })
      .catch(error => {
        dispatch(settingUpdateResponseAction({ error }));
        return reject({ error });
      });
  });

const settingDeleteResponseAction = ({ error, setting, data }) => ({
  type: SETTING_DELETE_RESPONSE,
  setting,
  data,
  error,
});

export const deleteSettings = ({ id }) => dispatch =>
  new Promise((resolve, reject) => {
    axios.delete(`${process.env.REACT_APP_API}/api/setting/${id}.json`)
      .then(({ data }) => {
        if (!data.success) {
          return reject({ error: data.success });
        }

        dispatch(settingDeleteResponseAction({ setting: { id }, data }));
        resolve({ ...data });
      })
      .catch(error => {
        dispatch(settingDeleteResponseAction({ error }));
        return reject({ error });
      });
  });