import axios from 'axios';
import { UMBRELLA_CREATE_RESPONSE, UMBRELLA_UPDATE_RESPONSE } from '../actionTypes';

require('dotenv').config();

const umbrellaCreateResponseAction = ({ error, umbrella, data }) => ({
  type: UMBRELLA_CREATE_RESPONSE,
  umbrella,
  data,
  error,
});

export const createUmbrella = ({ data }) => dispatch =>
  new Promise((resolve, reject) => {
    axios.post(`${process.env.REACT_APP_API}/api/umbrella.json`, { ...data })
      .then(({ data }) => {
        if (!data.success) {
          return reject({ error: data.success });
        }

        dispatch(umbrellaCreateResponseAction({ umbrella: data.umbrella, data }));
        resolve({ ...data });
      })
      .catch(error => {
        dispatch(umbrellaCreateResponseAction({ error }));
        return reject({ error });
      });
  });

const umbrellaUpdateResponseAction = ({ error, umbrella, data }) => ({
  type: UMBRELLA_UPDATE_RESPONSE,
  umbrella,
  data,
  error,
});

export const updateUmbrella = ({ id, data }) => dispatch =>
  new Promise((resolve, reject) => {
    axios.put(`${process.env.REACT_APP_API}/api/umbrella/${id}.json`, { ...data })
      .then(({ data }) => {
        if (!data.success) {
          return reject({ error: data.success });
        }

        dispatch(umbrellaUpdateResponseAction({ umbrella: data.umbrella, data }));
        resolve({ ...data });
      })
      .catch(error => {
        dispatch(umbrellaUpdateResponseAction({ error }));
        return reject({ error });
      });
  });