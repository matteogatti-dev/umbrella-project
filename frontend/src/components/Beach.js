import React, { Component } from 'react';
import { Container, Row, Col, Spinner, Button, Modal, Dropdown } from 'react-bootstrap';

import Umbrella from '../components/core/Umbrella';

import { BeachWrapper, Grid } from './Beach.theme';

class Beach extends Component {
  state = { isModalOpen: false, position_index: null, isLoading: false };

  onUmbrellaClick = ({ position_index }) => {
    this.setState({
      position_index,
      isModalOpen: true
    })
  };

  onUmbrellaStateChange = ({ state }) => {
    const { setting } = this.props;
    const { position_index } = this.state;

    const umbrella_positions = setting.umbrella_positions.split(',');
    umbrella_positions[position_index] = state;

    this.setState({ isLoading: true });

    this.props.onUpdateSetting({ id: setting.id, data: { umbrella_positions: umbrella_positions.join(',') } }).then(response => {
      if (response.success) {
        this.resetState()
      }
    })
  };

  resetState = () => {
    this.setState({
      isModalOpen: false,
      position_index: null,
      isLoading: false
    })
  };

  render() {
    const { setting, history } = this.props;
    const { isLoading, isModalOpen, position_index } = this.state;

    return (
      <BeachWrapper>
        <Container>
          {isModalOpen && (
            <Modal.Dialog className="Modal__custom">
              <Modal.Header>
                <Modal.Title>Umbrella #{position_index}</Modal.Title>
              </Modal.Header>

              {!isLoading && (
                <Modal.Body>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-variants-Secondary">
                      Imposta stato dell'ombrellone selezionato
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => this.onUmbrellaStateChange({ state: 0 })}>Disponibile</Dropdown.Item>
                      <Dropdown.Item onClick={() => this.onUmbrellaStateChange({ state: 1 })}>Prenotato</Dropdown.Item>
                      <Dropdown.Item onClick={() => this.onUmbrellaStateChange({ state: -1 })}>Non Prenotabile</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </Modal.Body>
              )}

              <Modal.Footer>
                <Button
                  variant="secondary"
                  onClick={() => this.resetState()}
                  disabled={isLoading}
                >
                  {isLoading ? <Spinner as="span" animation="grow" size="sm" role="status" /> : 'CHIUDI'}
                  </Button>
              </Modal.Footer>
            </Modal.Dialog>
          )}

          {!setting && (
            <Row className="text-center pt-5">
              <Col md={12}>
                <Spinner animation="border" />
              </Col>
            </Row>
          )}

          {setting && (
            <Row>
              <Col className="text-center pt-2">
                <h1>{setting.beach_name.toUpperCase()}</h1>
              </Col>
            </Row>
          )}
        </Container>

        <Grid>
          {setting && (
            <Grid.Content divider={setting.beach_columns}>
              {setting.umbrella_positions.split(',').map((umbrella_state, i) => {
                return (
                  <Grid.Element key={i}>
                    <Umbrella state={umbrella_state} onClick={() => {
                      if (parseInt(umbrella_state, 10) !== 1) this.onUmbrellaClick({ position_index: i })
                    }} />
                  </Grid.Element>
                )
              })}
            </Grid.Content>
          )}
        </Grid>

        {setting && (
          <Row className="text-center pt-5">
            <Col md={12}>
              <Button variant="primary" disabled={isLoading} onClick={() => history.push('/')}>
                RITORNA ALLA HOME
              </Button>
            </Col>
          </Row>
        )}
      </BeachWrapper>
    )
  }
}

export default Beach