import styled from '@emotion/styled';
import { css } from '@emotion/core'

export const BeachWrapper = styled('div')(
  () => css`
    .Modal__custom {
      position: absolute;
      width: '100%';
      z-index: 10;
      left: 0;
      right: 0;
      margin-left: auto;
      margin-right: auto;
    }
  `
);

export const Grid = styled('div')(
  () => css`
    display: block;
  `
);

const Content = styled('div')(
  ({ divider }) => css`
    display: grid;
    grid-template-columns: repeat(${divider}, 1fr);
    grid-gap: 10px;
  `
);

const Element = styled('div')(
  () => css`
    display: flex;
    justify-content: center;
    align-items: center;
  `
);

Grid.Content = Content;
Grid.Element = Element;