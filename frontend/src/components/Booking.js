import React, { Component } from 'react';
import {Container, Row, Col, Spinner, Button, Modal, Form, ButtonToolbar, Alert} from 'react-bootstrap';

import Umbrella from '../components/core/Umbrella';

import { BeachWrapper, Grid } from './Beach.theme';

class Booking extends Component {
  state = {
    isModalOpen: false,
    hasError: false,
    position_index: null,
    isLoading: false,
    booking_step: 0,
    data: {}
  };

  onUmbrellaClick = ({ position_index }) => {
    this.setState({
      position_index,
      isModalOpen: true
    })
  };

  handleCustomerChange = event => {
    const { data } = this.state;

    this.setState({
      data: {
        ...data,
        [event.target.name]: isNaN(event.target.value)
          ? event.target.value
          : parseFloat(event.target.value)
      }
    });
  };

  handleCustomerSubmit = event => {
    event.preventDefault();

    const { setting } = this.props;
    const { data, position_index, booking_step } = this.state;

    if (!data['first_name']) {
      this.setState({ hasError: true });

      return false;
    }

    if (booking_step === 0) {
      this.setState({ booking_step: 1 });

      return;
    }

    if (!data['price']) {
      this.setState({ hasError: true });

      return false;
    }

    this.setState({ isLoading: true });

    this.props.onCreateCustomer({
      data: {
        first_name: data['first_name'],
        ...(data['last_name'] ? { last_name: data['last_name'] } : {}),
        ...(data['phone'] ? { last_name: data['phone'] } : {})
      }
    }).then(response => {
      if (response.success) {
        this.props.onCreateUmbrella({
          data: {
            setting: setting.id,
            position_index,
            state: 1,
            price: parseFloat(data['price']),
            customer: response.customer.id,
            ...(data['start_reservation'] ? { last_name: data['start_reservation'] } : {}),
            ...(data['end_reservation'] ? { last_name: data['end_reservation'] } : {})
          }
        }).then(response => {
          if (response.success) {
            this.props.refreshSettings().then(() => this.resetState())
          }
        })
      }
    })
  };

  resetState = () => {
    this.setState({
      isModalOpen: false,
      hasError: false,
      position_index: null,
      isLoading: false,
      booking_step: 0
    })
  };

  render() {
    const { setting, history } = this.props;
    const { isLoading, isModalOpen, hasError, position_index, booking_step } = this.state;

    return (
      <BeachWrapper>
        <Container>
          {isModalOpen && (
            <Modal.Dialog className="Modal__custom">
              {hasError && (
                <Row className="mt-1 ml-1 mr-1">
                  <Col>
                    <Alert variant="danger" dismissible onClick={() => this.setState({ hasError: false })}>
                      Non tutti i campi sono compilati
                    </Alert>
                  </Col>
                </Row>
              )}

              <Modal.Header>
                <Modal.Title>Prenotazione di Umbrella #{position_index}</Modal.Title>
              </Modal.Header>

              {isLoading && (
                <Row className="text-center pt-1">
                  <Col md={12}>
                    <Spinner animation="border" />
                  </Col>
                </Row>
              )}

              {!isLoading && (
                <Modal.Body>
                  <Form onSubmit={e => this.handleCustomerSubmit(e)}>
                    {booking_step === 0 && (
                      <Form.Group>
                        <Form.Label>Nome</Form.Label>
                        <Form.Control name="first_name" placeholder="Mario" onChange={this.handleCustomerChange} />

                        <Form.Label>Cognome</Form.Label>
                        <Form.Control name="last_name" placeholder="Rossi" onChange={this.handleCustomerChange} />
                      </Form.Group>
                    )}

                    {booking_step === 1 && (
                      <Form.Group>
                        <Form.Label>Prezzo</Form.Label>
                        <Form.Control name="price" placeholder="12.5" onChange={this.handleCustomerChange} />

                        <Form.Label>Data Inizio</Form.Label>
                        <Form.Control name="start_reservation" type="date" onChange={this.handleCustomerChange} />

                        <Form.Label>Data Fine</Form.Label>
                        <Form.Control name="end_reservation" type="date" onChange={this.handleCustomerChange} />
                      </Form.Group>
                    )}

                    <ButtonToolbar>
                      <Button type="submit" variant="primary" disabled={isLoading}>
                        {booking_step > 0 ? 'SALVA' : 'CONTINUA'}
                      </Button>
                    </ButtonToolbar>
                  </Form>
                </Modal.Body>
              )}

              {!isLoading && (
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={() => this.resetState()}
                  >ANNULLA</Button>
                </Modal.Footer>
              )}
            </Modal.Dialog>
          )}

          {!setting && (
            <Row className="text-center pt-5">
              <Col md={12}>
                <Spinner animation="border" />
              </Col>
            </Row>
          )}

          {setting && (
            <Row>
              <Col className="text-center pt-2">
                <h1>{setting.beach_name.toUpperCase()}</h1>
              </Col>
            </Row>
          )}
        </Container>

        <Grid>
          {setting && (
            <Grid.Content divider={setting.beach_columns}>
              {setting.umbrella_positions.split(',').map((umbrella_state, i) => {
                return (
                  <Grid.Element key={i}>
                    <Umbrella state={umbrella_state} onClick={() => {
                      if (parseInt(umbrella_state, 10) === 0) {
                        this.onUmbrellaClick({ position_index: i })
                      }
                    }}/>
                  </Grid.Element>
                )
              })}
            </Grid.Content>
          )}
        </Grid>

        {setting && (
          <Row className="text-center pt-5">
            <Col md={12}>
              <Button variant="primary" disabled={isLoading} onClick={() => history.push('/')}>
                {isLoading ? <Spinner as="span" animation="grow" size="sm" role="status" /> : 'RITORNA ALLA HOME'}
              </Button>
            </Col>
          </Row>
        )}
      </BeachWrapper>
    )
  }
}

export default Booking