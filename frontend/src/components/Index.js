import React, { Component } from 'react';
import { Container, Row, Col, Spinner, Card, Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

class Index extends Component {
  render() {
    const { settings, settings_loading, history } = this.props;

    const hasNoSetting = (settings && settings.length === 0) || false;

    return (
      <Container>
        {settings_loading && (
          <Row className="text-center pt-5">
            <Col>
              <Spinner animation="border" />
            </Col>
          </Row>
        )}

        {!settings_loading && !hasNoSetting && (
          <Row className="pt-5">
            {settings.map(setting => {
              return (
                <Col key={setting.id} className="text-center">
                  <Card>
                    <Card.Body>
                      <Card.Title>{setting.beach_name.toUpperCase()}</Card.Title>
                      <Card.Text className="text-center">
                        <Button
                          variant="outline-primary"
                          onClick={() => history.push(`/setting/${setting.id}`)}
                        >MODIFICA</Button>

                        <Button
                          className="ml-1"
                          variant="outline-secondary"
                          onClick={() => history.push(`/beach/${setting.id}`)}
                        >CONFIGURA</Button>

                        <Button
                          className="ml-1"
                          variant="outline-info"
                          onClick={() => history.push(`/booking/${setting.id}`)}
                        >PRENOTAZIONI</Button>
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              )
            })}
          </Row>
        )}

        {!settings_loading && hasNoSetting && (
          <Row className="text-center pt-5">
            <Col md={12}>
              <Button variant="primary" onClick={() => history.push('/setting/new')}>CREA</Button>
            </Col>
          </Row>
        )}
      </Container>
    )
  }
}

export default withRouter(Index)