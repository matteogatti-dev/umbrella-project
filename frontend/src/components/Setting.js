import React, { Component } from 'react';
import { Container, Form, Row, Col, Button, Alert, Spinner, ButtonToolbar } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

class Setting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        ...props.setting
      },
      isLoading: false,
      hasError: false
    };
  }

  handleChange = event => {
    const { data } = this.state;

    this.setState({
      data: {
        ...data,
        [event.target.name]: isNaN(event.target.value)
          ? event.target.value
          : parseInt(event.target.value, 10)
      }
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    const { setting } = this.props;
    const { data } = this.state;

    const setting_data = {
      ...setting,
      ...data
    };

    if (!setting && (!setting_data['beach_name'] || !setting_data['beach_rows'] || !setting_data['beach_columns'])) {
      this.setState({ hasError: true });

      return false;
    }

    this.setState({ isLoading: true });

    if (!setting) {
      this.props.onCreateSetting({ data: setting_data }).then(response => {
        if (response.success) {
          this.props.history.push('/');
        }
      });

      return true;
    }

    this.props.onUpdateSetting({ id: setting.id, data: setting_data }).then(response => {
      if (response.success) {
        this.props.history.push('/')
      }
    });
  };

  render() {
    const { setting, history } = this.props;
    const { isLoading, hasError } = this.state;

    return (
      <Container>
        {hasError && (
          <Row className="pt-1">
            <Col>
              <Alert variant="danger" dismissible onClick={() => this.setState({ hasError: false })}>
                Non tutti i campi sono compilati
              </Alert>
            </Col>
          </Row>
        )}

        <Row className="pt-2">
          <Col>
            <Form onSubmit={e => this.handleSubmit(e)}>
              <Form.Group>
                <Form.Label>Nome chalet</Form.Label>
                <Form.Control
                  name='beach_name'
                  type="text"
                  placeholder="Es.: Chalet da Mario"
                  defaultValue={(setting && setting.beach_name) || ''}
                  onChange={this.handleChange}
                />
              </Form.Group>

              {!setting && (
                <Form.Group>
                  <Form.Label>Numero file ombrelloni</Form.Label>
                  <Form.Control name="beach_rows" type="num" placeholder="5" onChange={this.handleChange} />

                  <Form.Label>Numero colonne ombrelloni</Form.Label>
                  <Form.Control name="beach_columns" type="num" placeholder="10" onChange={this.handleChange} />
                </Form.Group>
              )}

              <ButtonToolbar>
                <Button type="submit" variant="primary" disabled={isLoading}>
                  {isLoading ? <Spinner as="span" animation="grow" size="sm" role="status" /> : 'SALVA'}
                </Button>

                <Button className="ml-1" variant="secondary" disabled={isLoading} onClick={() => history.push('/')}>ANNULLA</Button>

                {setting && (
                  <Button className="ml-1" variant="danger" disabled={isLoading} onClick={() => {
                    if (window.confirm('Confermi la cancellazione?')) {
                      this.setState({ isLoading: true });

                      this.props.onDeleteSetting({ id: setting.id }).then(response => {
                        if (response.success) {
                          history.push('/')
                        }
                      })
                    }
                  }}>
                    CANCELLA
                  </Button>
                )}
              </ButtonToolbar>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default withRouter(Setting)