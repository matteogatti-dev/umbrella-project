import React from 'react';
import Beach from '../Beach';
import renderer from 'react-test-renderer';

const setting = {
  id: 1,
  beach_name: "Chalet da Mario",
  beach_rows: 5,
  beach_columns: 10,
  umbrella_positions: "0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,-1,-1,0,0,0,0,0,0,0"
};

it('Beach renders correctly', () => {
  const tree = renderer.create(<Beach
    setting={setting}
    setting_loading={false}
  />).toJSON();

  expect(tree).toMatchSnapshot();
});