import React from 'react';
import Umbrella from './Umbrella.theme';

const Component = props => (
  <Umbrella {...props} />
);

export default Component;