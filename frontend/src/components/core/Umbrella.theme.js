import styled from '@emotion/styled';
import { css } from '@emotion/core'

const Umbrella = styled('div')(
  ({ state }) => css`
      width: 100px;
      height: 100px;
      overflow: hidden;
      border-radius: 50%;
      ${state === '1' && { background: '#FA5C4F' }}
      ${state === '0' && { background: '#ADE8BF' }}
      ${state === '-1' && { background: '#C2BFB8' }}
  `
);

export default Umbrella;
