import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { withRouter } from 'react-router-dom';

import Beach from '../components/Beach'

import { fetchSettingsIfNeeded, updateSettings } from '../actions/setting'
import { getSettingList, getSettinLoading } from '../selectors/setting.js'

class BeachContainer extends Component {
  componentDidMount = () => {
    this.props.getSettings()
  };

  render() {
    return <Beach {...this.props} />;
  }
}

const getSettingId = (state, props) => (props && props.match && props.match.params && props.match.params.id) || null;

const mapStateToProps = () =>
  createSelector(
    getSettingList,
    getSettinLoading,
    getSettingId,
    (settings, settings_loading, setting_id) => {
      return {
        setting: settings.find(s => s.id === parseInt(setting_id, 10)),
        settings_loading
      };
    }
  );

const mapDispatchToProps = dispatch => {
  return {
    getSettings() {
      dispatch(fetchSettingsIfNeeded())
    },
    onUpdateSetting({ id, data }) {
      return dispatch(updateSettings({ id, data }))
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BeachContainer));
