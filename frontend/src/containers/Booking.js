import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import Booking from '../components/Booking'

import { fetchSettingsIfNeeded, fetchSettings } from '../actions/setting'
import { createCustomer } from '../actions/customer'
import { createUmbrella } from '../actions/umbrella'
import { getSettingList, getSettinLoading } from '../selectors/setting.js'
import { withRouter } from 'react-router-dom';

class BookingContainer extends Component {
  componentDidMount = () => {
    this.props.getSettings()
  };

  render() {
    return <Booking {...this.props} />;
  }
}

const getSettingId = (state, props) => (props && props.match && props.match.params && props.match.params.id) || null;

const mapStateToProps = () =>
  createSelector(
    getSettingList,
    getSettinLoading,
    getSettingId,
    (settings, settings_loading, setting_id) => {
      return {
        setting: settings.find(s => s.id === parseInt(setting_id, 10)),
        settings_loading
      };
    }
  );

const mapDispatchToProps = dispatch => {
  return {
    getSettings() {
      dispatch(fetchSettingsIfNeeded())
    },
    refreshSettings() {
      return dispatch(fetchSettings())
    },
    onCreateCustomer({ data }) {
      return dispatch(createCustomer(({ data })))
    },
    onCreateUmbrella({ data }) {
      return dispatch(createUmbrella({ data }))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BookingContainer));
