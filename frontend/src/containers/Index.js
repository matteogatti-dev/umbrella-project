import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import Index from '../components/Index'

import { fetchSettingsIfNeeded } from '../actions/setting'
import { getSettingList, getSettinLoading } from '../selectors/setting.js'

class IndexContainer extends Component {
  componentDidMount = () => {
    this.props.getSettings()
  };

  render() {
    return <Index {...this.props} />;
  }
}

const mapStateToProps = () =>
  createSelector(
    getSettingList,
    getSettinLoading,
    (settings, settings_loading) => {
      return {
        settings,
        settings_loading
      };
    }
  );

const mapDispatchToProps = dispatch => {
  return {
    getSettings() {
      dispatch(fetchSettingsIfNeeded())
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(IndexContainer);
