import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import Setting from '../components/Setting'

import { fetchSettingsIfNeeded, createSettings, updateSettings, deleteSettings } from '../actions/setting'
import { getSettingList, getSettinLoading } from '../selectors/setting.js'

class SettingContainer extends Component {
  componentDidMount = () => {
    this.props.getSettings()
  };

  render() {
    return <Setting {...this.props} />;
  }
}

const getSettingId = (state, props) => (props && props.match && props.match.params && props.match.params.id) || null;

const mapStateToProps = () =>
  createSelector(
    getSettingList,
    getSettinLoading,
    getSettingId,
    (settings, settings_loading, setting_id) => {
      return {
        setting: settings.find(s => s.id === parseInt(setting_id, 10)),
        settings_loading
      };
    }
  );

const mapDispatchToProps = dispatch => {
  return {
    getSettings() {
      dispatch(fetchSettingsIfNeeded())
    },
    onCreateSetting({ data }) {
      return dispatch(createSettings({ data }))
    },
    onUpdateSetting({ id, data }) {
      return dispatch(updateSettings({ id, data }))
    },
    onDeleteSetting({ id }) {
      return dispatch(deleteSettings({ id }))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingContainer);
