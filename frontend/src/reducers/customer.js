import { CUSTOMER_CREATE_RESPONSE } from '../actionTypes';

export const setting = (state = {
    isFetching: false,
    didInvalidate: false,
    items: []
  }, action) => {
  switch (action.type) {
    case CUSTOMER_CREATE_RESPONSE:
      if (!action.customer) {
        return state;
      }

      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: [].concat(state.items).concat(action.customer),
      };

    default:
      return state;
  }
};
