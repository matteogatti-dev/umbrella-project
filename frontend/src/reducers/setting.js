import {
  SETTING_LIST_REQUEST,
  SETTING_LIST_RESPONSE,
  SETTING_CREATE_RESPONSE,
  SETTING_UPDATE_RESPONSE,
  SETTING_DELETE_RESPONSE
} from '../actionTypes';

export const setting = (state = {
    isFetching: false,
    didInvalidate: false,
    items: []
  }, action) => {
  switch (action.type) {
    case SETTING_LIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false
      };

    case SETTING_LIST_RESPONSE:
      if (!action.settings) {
        return state;
      }

      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: action.settings,
      };

    case SETTING_CREATE_RESPONSE:
      if (!action.setting) {
        return state;
      }

      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: [].concat(state.items).concat(action.setting),
      };

    case SETTING_UPDATE_RESPONSE:
      if (!action.setting) {
        return state;
      }

      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: []
          .concat(state.items)
          .concat(action.setting)
          .reduce((list, item) => {
            list = list.filter(({ id }) => id !== item.id);
            list.push(item);

            return list;
          }, []),
      };

    case SETTING_DELETE_RESPONSE:
      if (!action.setting) {
        return state;
      }

      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items:
          []
            .concat(state.items || [])
            .filter(({ id }) => id !== action.setting.id)
      };

    default:
      return state;
  }
};
