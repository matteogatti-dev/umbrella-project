import { UMBRELLA_CREATE_RESPONSE, UMBRELLA_UPDATE_RESPONSE } from '../actionTypes';

export const setting = (state = {
    isFetching: false,
    didInvalidate: false,
    items: []
  }, action) => {
  switch (action.type) {
    case UMBRELLA_CREATE_RESPONSE:
      if (!action.umbrella) {
        return state;
      }

      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: [].concat(state.items).concat(action.umbrella),
      };

    case UMBRELLA_UPDATE_RESPONSE:
      if (!action.umbrella) {
        return state;
      }

      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: []
          .concat(state.items)
          .concat(action.umbrella)
          .reduce((list, item) => {
            list = list.filter(({ id }) => id !== item.id);
            list.push(item);

            return list;
          }, []),
      };

    default:
      return state;
  }
};
