import { createSelector } from 'reselect';

export const getSetting = state => state.setting;

export const getSettingList = createSelector(
  [getSetting],
  (setting) => {
    return (setting && setting.items) || [];
  }
);

export const getSettinLoading = createSelector(
  [getSetting], setting => {
    return (setting && setting.isFetching) || false;
  }
);
